#!/bin/bash

function gen_key {
    echo "gen_key $@"
    PUID="${1}"
    shift 1
    cat >/tmp/keygen <<EOF
%no-protection
%no-ask-passphrase
%transient-key
Key-Type: EDDSA
Key-Curve: ed25519
Subkey-Type: ECDH
Subkey-Curve: cv25519
Expire-Date: 0
Name-Real: Mutlischleuder Test User
Name-Comment: TEST KEY DO NOT USE
Name-Email: ${PUID}
EOF
    gpg --batch --full-gen-key /tmp/keygen
    for uid in $@; do
	gpg --batch --quick-add-uid "${PUID}" "Mutlischleuder Test User (TEST KEY DO NOT USE) <${uid}>"
    done
    gpg --export --armor "${PUID}" > "/tmp/${PUID}.asc"
    for uid in $@; do
	gpg --export --armor "${uid}" > "/tmp/${uid}.asc"
    done
}

function subscribe {
    schleuder-cli subscriptions new "${1}" "${2}" "/tmp/${2}.asc"
}

gen_key admin@example.org
gen_key admin2@example.org
gen_key ada.lovelace@example.org
gen_key alex.example@example.org
gen_key aspammer@example.org
gen_key anna.example@example.org
mv /tmp/anna.example@example.org.asc /tmp/anna.example@example.org.old.asc

gen_key anotherspammer@example.org
gen_key andy.example@example.org
mv /tmp/andy.example@example.org.asc /tmp/andy.example@example.org.1.asc
gen_key aaron.example@example.org aaron.example@example.net
gen_key amy.example@example.org
gen_key alice.example@example.org alice.example@example.net

install -m 0700 -d /tmp/gpg
export GNUPGHOME=/tmp/gpg
gen_key anna.example@example.org
gen_key andy.example@example.org
unset GNUPGHOME

schleuder-cli lists new test@schleuder.example.org admin@example.org /tmp/admin@example.org.asc
schleuder-cli lists new test-global@schleuder.example.org admin@example.org /tmp/admin@example.org.asc
schleuder-cli lists new test-north@schleuder.example.org admin@example.org /tmp/admin@example.org.asc
schleuder-cli lists new test-east@schleuder.example.org admin@example.org /tmp/admin@example.org.asc
schleuder-cli lists new test-south@schleuder.example.org admin@example.org /tmp/admin@example.org.asc
schleuder-cli lists new test-west@schleuder.example.org admin2@example.org /tmp/admin2@example.org.asc
schleuder-cli lists new test2-global@schleuder.example.org admin2@example.org /tmp/admin2@example.org.asc

subscribe test-global@schleuder.example.org ada.lovelace@example.org      # should be unsubscribed 
subscribe test-global@schleuder.example.org aaron.example@example.org     # should remain as-is
subscribe test-global@schleuder.example.org aaron.example@example.net     # should be unsubscribed, but key should remain
subscribe test-global@schleuder.example.org alex.example@example.org      # should remain as-is
schleuder-cli subscriptions new test-global@schleuder.example.org anna.example@example.org /tmp/anna.example@example.org.old.asc
                                                                          # key should be updated
subscribe test-global@schleuder.example.org aspammer@example.org          # should be unsubscribed

subscribe test-north@schleuder.example.org alex.example@example.org       # should remain as-is
subscribe test-north@schleuder.example.org aspammer@example.org           # should be ignored
schleuder-cli subscriptions new test-north@schleuder.example.org arno.example@example.org
                                                                          # should not be subscribed - no key

subscribe test-east@schleuder.example.org anna.example@example.org        # key should be updated
subscribe test-east@schleuder.example.org anotherspammer@example.org      # should not be subscribed
subscribe test-east@schleuder.example.org aaron.example@example.org       # should remain as-is
subscribe test-east@schleuder.example.org alice.example@example.net       # should be subscriped despite conflict

subscribe test-south@schleuder.example.org andy.example@example.org       # should be subscribed despite key conflict
subscribe test-south@schleuder.example.org amy.example@example.org        # should be subscribed - conflict but same key

sleep 5  # to get different subscription dates

schleuder-cli subscriptions new test-west@schleuder.example.org andy.example@example.org /tmp/andy.example@example.org.1.asc
                                                                          # should not be subscribed
subscribe test-west@schleuder.example.org amy.example@example.org         # should be subscribed - conflict but same key
subscribe test-west@schleuder.example.org alice.example@example.org       # should not be subscriped - key used by other UID

schleuder-cli subscriptions new test2-global@schleuder.example.org arno.example@example.org
                                                                          # should be unsubscribed - no key
