from setuptools import setup, find_packages

from multischleuder import __version__


setup(
    name='multischleuder',
    version=__version__,
    author='s3lph',
    author_email='1375407-s3lph@users.noreply.gitlab.com',
    description='Merge subscribers and keys of multiple Schleuder lists into one',
    license='MIT',
    keywords='schleuder,pgp',
    url='https://gitlab.com/s3lph/multischleuder',
    packages=find_packages(exclude=['*.test']),
    install_requires=[
        'python-dateutil',
        'PyYAML',
        'PGPy',
    ],
    entry_points={
        'console_scripts': [
            'multischleuder = multischleuder.main:main'
        ]
    },
)
