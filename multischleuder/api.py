
from typing import List, Optional

import base64
import json
import logging
import os
import ssl
import urllib.error
import urllib.request

from multischleuder.types import SchleuderKey, SchleuderList, SchleuderSubscriber


class SchleuderApi:

    def __init__(self,
                 url: str,
                 token: str,
                 cafile: Optional[str] = None):
        self._url = url
        self._cafile = cafile
        self._dry_run = False
        # The API token is not used as a Bearer token, but as the
        # password for the static "schleuder" user.
        auth = base64.b64encode(f'schleuder:{token}'.encode()).decode()
        self._headers = {
            'Authorization': f'Basic {auth}'
        }

    def __request(self,
                  path: str,
                  list_id: Optional[int] = None,
                  data: Optional[str] = None,
                  method: str = 'GET',
                  fmt=[]):
        # Compose and render fully-qualified URL
        if fmt:
            path = path.format(*fmt)
        url = os.path.join(self._url, path)
        if list_id is not None:
            url += f'?list_id={list_id}'

        payload: Optional[bytes] = data.encode() if data is not None else None
        # Create a custom SSL context that does not validate the hostname, but
        # validates against the self-signed schleuder-api-daemon certificate
        if self._cafile is not None:
            context = ssl.create_default_context(cafile=self._cafile)
            context.check_hostname = False
        else:
            context = None
        # Perform the actual request
        req = urllib.request.Request(url, data=payload, method=method, headers=self._headers)
        resp = urllib.request.urlopen(req, context=context)  # nosec B310 baseurl is trusted
        respdata: str = resp.read().decode()
        if len(respdata) > 0:
            return json.loads(respdata)
        return None

    def dry_run(self):
        self._dry_run = True

    # List Management

    def get_lists(self) -> List[SchleuderList]:
        lists = self.__request('lists.json')
        return [SchleuderList.from_api(**s) for s in lists]

    def get_list_admins(self, schleuder: SchleuderList) -> List[SchleuderSubscriber]:
        response = self.__request('subscriptions.json', schleuder.id)
        admins: List[SchleuderSubscriber] = []
        for r in response:
            if not r['admin']:
                continue
            key: Optional[SchleuderKey] = None
            if r['fingerprint']:
                key = self.get_key(r['fingerprint'], schleuder)
            admin = SchleuderSubscriber.from_api(key, **r)
            admins.append(admin)
        return admins

    # Subscriber Management

    def get_subscribers(self, schleuder: SchleuderList) -> List[SchleuderSubscriber]:
        response = self.__request('subscriptions.json', schleuder.id)
        subs: List[SchleuderSubscriber] = []
        for r in response:
            key: Optional[SchleuderKey] = None
            if r['fingerprint']:
                key = self.get_key(r['fingerprint'], schleuder)
            sub = SchleuderSubscriber.from_api(key, **r)
            subs.append(sub)
        return subs

    def get_subscriber(self, email: str, schleuder: SchleuderList) -> SchleuderSubscriber:
        response = self.__request('subscriptions.json', list_id=schleuder.id)
        for r in response:
            if r['email'] != email:
                continue
            key: Optional[SchleuderKey] = None
            if r['fingerprint']:
                key = self.get_key(r['fingerprint'], schleuder)
            return SchleuderSubscriber.from_api(key, **r)
        raise KeyError(f'{email} is not subscribed to {schleuder.name}')

    def subscribe(self, sub: SchleuderSubscriber, schleuder: SchleuderList):
        if sub.key is None:
            raise ValueError('Cannot subscribe without key')
        if self._dry_run:
            return
        data = json.dumps({
            'email': sub.email,
            'fingerprint': sub.key.fingerprint
        })
        self.__request('subscriptions.json', list_id=schleuder.id, data=data, method='POST')

    def unsubscribe(self, sub: SchleuderSubscriber, schleuder: SchleuderList):
        listsub = self.get_subscriber(sub.email, schleuder)
        if self._dry_run:
            return
        self.__request('subscriptions/{}.json', fmt=[listsub.id], method='DELETE')

    def update_fingerprint(self, sub: SchleuderSubscriber, schleuder: SchleuderList):
        if sub.key is None:
            raise ValueError('Cannot update fingerprint without key')
        listsub = self.get_subscriber(sub.email, schleuder)
        if self._dry_run:
            return
        data = json.dumps({'fingerprint': sub.key.fingerprint})
        self.__request('subscriptions/{}.json', fmt=[listsub.id], data=data, method='PATCH')

    # Key Management

    def get_key(self, fpr: str, schleuder: SchleuderList) -> Optional[SchleuderKey]:
        try:
            key = self.__request('keys/{}.json', list_id=schleuder.id, fmt=[fpr])
        except urllib.error.HTTPError as e:
            logging.exception(e)
            return None
        return SchleuderKey.from_api(schleuder.id, **key)

    def post_key(self, key: SchleuderKey, schleuder: SchleuderList):
        if self._dry_run:
            return
        data = json.dumps({
            'keymaterial': key.blob
        })
        self.__request('keys.json', list_id=schleuder.id, data=data, method='POST')

    def delete_key(self, key: SchleuderKey, schleuder: SchleuderList):
        if self._dry_run:
            return
        self.__request('keys/{}.json', list_id=schleuder.id, fmt=[key.fingerprint], method='DELETE')
