
from typing import Any, Dict, List, Tuple

import argparse
import logging
import sys

import yaml

from multischleuder import __version__
from multischleuder.api import SchleuderApi
from multischleuder.conflict import KeyConflictResolution
from multischleuder.processor import MultiList
from multischleuder.reporting import Reporter
from multischleuder.smtp import SmtpClient


def parse_list_config(api: SchleuderApi,
                      kcr: KeyConflictResolution,
                      config: Dict[str, Any]) -> 'MultiList':
    target = config['target']
    default_from = target.replace('@', '-owner@')
    mail_from = config.get('from', default_from)
    banned = config.get('banned', [])
    unmanaged = config.get('unmanaged', [])
    reporter = Reporter(
        send_admin_reports=config.get('send_admin_reports', True),
        send_conflict_messages=config.get('send_conflict_messages', True))
    return MultiList(
        sources=config['sources'],
        target=target,
        banned=banned,
        unmanaged=unmanaged,
        mail_from=mail_from,
        api=api,
        kcr=kcr,
        reporter=reporter
    )


def parse_config(ns: argparse.Namespace) -> Tuple[List['MultiList'], SmtpClient]:
    with open(ns.config, 'r') as f:
        c = yaml.safe_load(f)

    api = SchleuderApi(**c['api'])

    smtp_config = c.get('smtp', {})
    smtp = SmtpClient.parse(smtp_config)

    kcr_config = c.get('conflict', {})
    kcr = KeyConflictResolution(**kcr_config)

    if ns.dry_run:
        api.dry_run()
        kcr.dry_run()
        smtp.dry_run()

    lists = []
    for clist in c.get('lists', []):
        ml = parse_list_config(api, kcr, clist)
        lists.append(ml)
    return lists, smtp


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('--config', '-c', type=str, default='/etc/multischleuder/config.yml')
    ap.add_argument('--dry-run', '-n', action='store_true', default=False)
    ap.add_argument('--verbose', '-v', action='store_true', default=False)
    ap.add_argument('--version', action='version', version=__version__)
    ns = ap.parse_args(sys.argv[1:])
    level: str = logging.DEBUG if ns.verbose else logging.INFO
    logging.basicConfig(style='{', format='{asctime:s} [{levelname:^8s}] {message:}', level=level)
    logging.debug('Verbose logging enabled')
    lists, smtp = parse_config(ns)
    for lst in lists:
        try:
            lst.process(ns.dry_run)
        except BaseException:
            logging.exception(f'An error occurred while processing {lst._target}')
    smtp.send_messages(Reporter.get_messages())
