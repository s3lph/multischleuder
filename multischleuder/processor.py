
from typing import Dict, List, Set, Tuple

import logging

from multischleuder.api import SchleuderApi
from multischleuder.conflict import KeyConflictResolution
from multischleuder.reporting import AdminReport, Message, Reporter
from multischleuder.types import SchleuderKey, SchleuderList, SchleuderSubscriber


class MultiList:

    def __init__(self,
                 sources: List[str],
                 target: str,
                 unmanaged: List[str],
                 banned: List[str],
                 mail_from: str,
                 api: SchleuderApi,
                 kcr: KeyConflictResolution,
                 reporter: Reporter):
        self._sources: List[str] = sources
        self._target: str = target
        self._unmanaged: List[str] = unmanaged
        self._banned: List[str] = banned
        self._mail_from: str = mail_from
        self._api: SchleuderApi = api
        self._kcr: KeyConflictResolution = kcr
        self._reporter: Reporter = reporter

    def process(self, dry_run: bool = False):
        logging.info(f'Processing: {self._target} {"DRY RUN" if dry_run else ""}')
        target_list, sources = self._lists_by_name()
        sourcemap: Dict[int, str] = {s.id: s.name for s in sources}
        target_admins = self._api.get_list_admins(target_list)
        # Get current subs, except for unmanaged adresses
        current_subs: Set[SchleuderSubscriber] = set()
        for s in self._api.get_subscribers(target_list):
            if s.email in self._unmanaged or s.email == self._target:
                continue
            if s.key is not None and s.key.fingerprint == target_list.fingerprint:
                continue
            current_subs.add(s)
        current_keys: Set[SchleuderKey] = {s.key for s in current_subs if s.key is not None}
        all_subs: List[SchleuderSubscriber] = []
        # This loop may return multiple subscriptions for some users if they are subscribed on multiple sub-lists ...
        for source in sources:
            subs: List[SchleuderSubscriber] = self._api.get_subscribers(source)
            for s in subs:
                if s.email in self._banned or s.email in self._unmanaged or s.email == self._target:
                    continue
                if s.key is not None and s.key.fingerprint == target_list.fingerprint:
                    continue
                all_subs.append(s)
        # ... which is taken care of by the key conflict resolution routine
        resolved, conflicts = self._kcr.resolve(self._target, self._mail_from, all_subs, sourcemap)
        self._reporter.add_messages(conflicts)
        intended_subs: Set[SchleuderSubscriber] = set(resolved)
        intended_keys: Set[SchleuderKey] = {s.key for s in intended_subs if s.key is not None}
        # Determine the change set
        to_subscribe = intended_subs.difference(current_subs)
        to_unsubscribe = current_subs.difference(intended_subs)
        to_remove = current_keys.difference(intended_keys)
        to_add = intended_keys.difference(current_keys)
        # Already present keys that are being updated have to be removed and re-imported for convergence
        to_pre_remove = {k for k in to_add if k.fingerprint in {o.fingerprint for o in to_remove}}
        to_remove = {k for k in to_remove if k.fingerprint not in {o.fingerprint for o in to_pre_remove}}
        to_update = {s for s in intended_subs if s in current_subs and s.key in to_add}
        # Perform the actual list modifications in an order which avoids race conditions
        for key in to_pre_remove:
            self._api.delete_key(key, target_list)
            logging.info(f'Pre-removed key:   {key}')
        for key in to_add:
            self._api.post_key(key, target_list)
            logging.info(f'Added key:         {key}')
        for sub in to_subscribe:
            self._api.subscribe(sub, target_list)
            logging.info(f'Subscribed user:   {sub}')
        for sub in to_update:
            self._api.update_fingerprint(sub, target_list)
            logging.info(f'Updated key:       {sub}')
        for sub in to_unsubscribe:
            self._api.unsubscribe(sub, target_list)
            logging.info(f'Unsubscribed user: {sub}')
        for key in to_remove:
            self._api.delete_key(key, target_list)
            logging.info(f'Removed key:       {key}')

        # Workaround for quirky gpg behaviour where some key signatures are exported from a sublist, but dropped on
        # import into the target list, leading to a situation where the same key is imported over and over again.
        new_subs = set()
        # Get the new list of subscribers
        for s in self._api.get_subscribers(target_list):
            if s.email in self._unmanaged or s.email == self._target:
                continue
            if s.key is None or s.key.fingerprint == target_list.fingerprint:
                continue
            new_subs.add(s)
        # Compare the key blobs to the ones present before this run
        old_keys = {s.key.blob for s in current_subs if s.key is not None}
        unchanged_subs = {s for s in new_subs if s.key is not None and s.key.blob in old_keys}
        unchanged_fprs = {s.key.fingerprint for s in unchanged_subs if s.key is not None}
        # Remove the unchanged keys from the changesets so that they are not included in the admin report
        to_subscribe = {s for s in to_subscribe if s not in unchanged_subs}
        to_update = {s for s in to_update if s not in unchanged_subs}
        # need to compare by fpr because == includes the (potentially different) blob
        to_add = {k for k in to_add if k.fingerprint not in unchanged_fprs}

        if len(to_add) + len(to_subscribe) + len(to_unsubscribe) + len(to_remove) == 0:
            logging.info(f'No changes for {self._target}')
        else:
            for admin in target_admins:
                try:
                    report = AdminReport(self._target, admin.email, self._mail_from,
                                         admin.key.blob if admin.key is not None else None,
                                         to_subscribe, to_unsubscribe, to_update, to_add, to_remove,
                                         conflicts, sourcemap)
                    self._reporter.add_message(report)
                except BaseException:
                    logging.exception(f'Encryption to {admin.email} failed, not sending report')
        logging.info(f'Finished processing: {self._target}')

    def _lists_by_name(self) -> Tuple[SchleuderList, List[SchleuderList]]:
        lists = self._api.get_lists()
        target = [x for x in lists if x.name == self._target][0]
        sources = [x for x in lists if x.name in self._sources]
        return target, sources
