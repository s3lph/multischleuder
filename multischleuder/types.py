
from typing import List, Optional

from dataclasses import dataclass, field, Field
from datetime import datetime
from dateutil.parser import isoparse


@dataclass(frozen=True)
class SchleuderList:
    id: int = field(compare=True)
    name: str = field(compare=False)
    fingerprint: str = field(compare=False)

    @staticmethod
    def from_api(id: int,
                 email: str,
                 fingerprint: str,
                 *args, **kwargs) -> 'SchleuderList':
        return SchleuderList(id, email, fingerprint)


@dataclass(frozen=True)
class SchleuderSubscriber:
    id: int = field(compare=False)
    email: str = field(compare=True)
    key: Optional['SchleuderKey'] = field(compare=False)
    schleuder: int = field(compare=False)
    created_at: datetime = field(compare=False)

    @staticmethod
    def from_api(key: Optional['SchleuderKey'],
                 id: int,
                 list_id: int,
                 email: str,
                 created_at: str,
                 *args, **kwargs) -> 'SchleuderSubscriber':
        created = isoparse(created_at)
        return SchleuderSubscriber(id, email, key, list_id, created)

    def __str__(self) -> str:
        return f'{self.email}'


@dataclass(frozen=True)
class SchleuderKey:
    fingerprint: str = field(compare=True)
    email: str = field(compare=True)
    blob: str = field(compare=True, hash=False, repr=False)
    schleuder: int = field(compare=False)

    @staticmethod
    def from_api(schleuder: int,
                 fingerprint: str,
                 email: str,
                 ascii: str,
                 *args, **kwargs) -> 'SchleuderKey':
        lines: List[str] = []
        state = 0
        for line in ascii.splitlines():
            if '-----BEGIN PGP ' in line:
                state = 1
            if state == 1:
                lines.append(line)
            if '-----END PGP ' in line:
                state = 1
        return SchleuderKey(fingerprint, email, '\n'.join(lines), schleuder)

    def __str__(self) -> str:
        return f'{self.fingerprint} ({self.email})'
