
import unittest
from unittest.mock import MagicMock, mock_open, patch

from multischleuder.main import parse_config, parse_list_config
from multischleuder.test.test_api import _KEY_RESPONSE, _LIST_RESPONSE, _SUBSCRIBER_RESPONSE


_MINIMAL = '''
api:
  url: https://localhost:4443
  token: securetoken
conflict:
  interval: 3600
  statefile: /tmp/state.json
  key_template: ''
  user_template: ''
'''


_CONFIG = '''---

api:
  url: https://localhost:4443
  token: securetoken
  cafile: /tmp/ca.pem

smtp:
  hostname: smtp.example.org
  port: 26
  tls: STARTTLS
  username: multischleuder@example.org
  password: supersecurepassword

conflict:
  interval: 3600
  statefile: /tmp/state.json
  key_template: |
    Dear {subscriber}
    This is a test and should not be used in production:
    {affected}
    If you ever receive this text via email, notify your admin:
    {chosen}
    Regards, {schleuder}
  user_template: |
    Dear {subscriber}
    This is a test and should not be used in production:
    {affected}
    If you ever receive this text via email, notify your admin:
    {chosen}
    Kind regards, {schleuder}

lists:

  - target: test-global@schleuder.example.org
    from: test-global-owner@schleuder.example.org
    sources:
      - test-north@schleuder.example.org
      - test-east@schleuder.example.org
      - test-south@schleuder.example.org
      - test-west@schleuder.example.org
    unmanaged:
      - admin@example.org
    banned:
      - aspammer@example.org
      - anotherspammer@example.org

  - target: test2-global@schleuder.example.org
    from: test2-global-owner@schleuder.example.org
    sources:
      - test-north@schleuder.example.org
      - test-south@schleuder.example.org
    unmanaged:
      - admin@example.org
      - admin2@example.org
    banned: []

'''


class TestConfig(unittest.TestCase):

    def test_parse_minimal_config(self):
        ns = MagicMock()
        ns.config = '/tmp/config.yml'
        ns.dry_run = False
        ns.verbose = False
        with patch('builtins.open', mock_open(read_data=_MINIMAL)) as mock:
            lists, _ = parse_config(ns)
        self.assertEqual(0, len(lists))

    def test_parse_config(self):
        ns = MagicMock()
        ns.config = '/tmp/config.yml'
        ns.dry_run = False
        ns.verbose = False
        with patch('builtins.open', mock_open(read_data=_CONFIG)) as mock:
            lists, smtp = parse_config(ns)

        self.assertEqual(2, len(lists))
        list1, list2 = lists
        self.assertEqual('https://localhost:4443', list1._api._url)
        self.assertEqual('Basic c2NobGV1ZGVyOnNlY3VyZXRva2Vu', list2._api._headers['Authorization'])
        self.assertEqual('/tmp/ca.pem', list1._api._cafile)
        self.assertEqual(False, list1._api._dry_run)
        self.assertEqual(3600, list2._kcr._interval)
        self.assertEqual('/tmp/state.json', list1._kcr._state_file)
        self.assertIn('Regards, {schleuder}', list2._kcr._key_template)
        self.assertIn('Kind regards, {schleuder}', list2._kcr._user_template)

        self.assertEqual('test-global@schleuder.example.org', list1._target)
        self.assertEqual('test-global-owner@schleuder.example.org', list1._mail_from)
        self.assertEqual(['admin@example.org'], list1._unmanaged)
        self.assertEqual(['aspammer@example.org', 'anotherspammer@example.org'], list1._banned)
        self.assertEqual(4, len(list1._sources))
        self.assertIn('test-north@schleuder.example.org', list1._sources)
        self.assertIn('test-east@schleuder.example.org', list1._sources)
        self.assertIn('test-south@schleuder.example.org', list1._sources)
        self.assertIn('test-west@schleuder.example.org', list1._sources)

        self.assertEqual('test2-global@schleuder.example.org', list2._target)
        self.assertEqual('test2-global-owner@schleuder.example.org', list2._mail_from)
        self.assertEqual(['admin@example.org', 'admin2@example.org'], list2._unmanaged)
        self.assertEqual([], list2._banned)
        self.assertEqual(2, len(list2._sources))
        self.assertIn('test-north@schleuder.example.org', list2._sources)
        self.assertIn('test-south@schleuder.example.org', list2._sources)

        self.assertEqual('smtp+starttls://multischleuder@example.org@smtp.example.org:26', str(smtp))

    def test_parse_dry_run(self):
        ns = MagicMock()
        ns.config = '/tmp/config.yml'
        ns.dry_run = True
        ns.verbose = False
        with patch('builtins.open', mock_open(read_data=_CONFIG)) as mock:
            lists, _ = parse_config(ns)
        self.assertEqual(True, lists[0]._api._dry_run)
