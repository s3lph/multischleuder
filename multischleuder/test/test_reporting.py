
import unittest

from datetime import datetime

import pgpy.errors  # type: ignore

from multischleuder.reporting import KeyConflictMessage, AdminReport, Reporter, UserConflictMessage
from multischleuder.types import SchleuderKey, SchleuderList, SchleuderSubscriber
from multischleuder.test.test_conflict import _PRIVKEY_1


BROKENKEY = '''
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEYmcMbxYJKwYBBAHaRw8BAQdAKUohRdnuTSldKwawfLdwwUvOJjz/pHx3fXS2
v2dUQx+0SU11bHRpc2NobGV1ZGVyIEJyb2tlbiBBZG1pbiBLZXkgKFRFU1QgS0VZ
IERPIE5PVCBVU0UpIDxhZG1pbkBleGFtcGxlLm9yZz6IkAQTFggAOBYhBGtuFOnz
PJOCOdfv6OuAwhfh1Uj8BQJiZwxvAhsBBQsJCAcCBhUKCQgLAgQWAgMBAh4BAheA
AAoJEOuAwhfh1Uj8PnkBAM6PfYUZbvvYEkSdwzmZXDwhPRsSA0bhjL5aVwIeCCdp
AQDeImNI6czSLVAuwObKv8FnpmbFi3HxTNzakp44DoD8Aw==
=JtdI
-----END PGP PUBLIC KEY BLOCK-----
'''


def one_of_each_kind():
    sub = SchleuderSubscriber(1, 'foo@example.org', None, 1, datetime.utcnow())
    key = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), 1)
    sub2 = SchleuderSubscriber(2, 'bar@example.org', key, 1, datetime.utcnow())
    msg1 = KeyConflictMessage(
        schleuder='test@example.org',
        chosen=sub,
        affected=[sub],
        digest='digest',
        mail_from='test-owner@example.org',
        template='averylongmessage',
        sourcemap={1: 'test@example.org'})
    msg2 = AdminReport(
        schleuder='test@example.org',
        mail_to='admin@example.org',
        mail_from='test-owner@example.org',
        encrypt_to=str(_PRIVKEY_1.pubkey),
        subscribed={},
        unsubscribed={sub},
        updated={},
        added={},
        removed={},
        conflicts=[],
        sourcemap={1: 'test@example.org'})
    msg3 = UserConflictMessage(
        schleuder='test@example.org',
        subscriber='bar@example.org',
        chosen=sub2,
        affected=[sub2],
        digest='digest',
        mail_from='test-owner@example.org',
        template='averylongmessage',
        sourcemap={1: 'test@example.org'})
    return [msg1, msg2, msg3]


class TestReporting(unittest.TestCase):

    def test_reporter_config_all_enabled(self):
        msgs = one_of_each_kind()
        r = Reporter(send_conflict_messages=True,
                     send_admin_reports=True)
        r.add_messages(msgs)
        self.assertEqual(3, len(Reporter.get_messages()))
        self.assertIsInstance(Reporter.get_messages()[0], KeyConflictMessage)
        self.assertEqual('foo@example.org', Reporter.get_messages()[0]._to)
        self.assertIsInstance(Reporter.get_messages()[1], AdminReport)
        self.assertEqual('admin@example.org', Reporter.get_messages()[1]._to)
        self.assertIsInstance(Reporter.get_messages()[2], UserConflictMessage)
        self.assertEqual('bar@example.org', Reporter.get_messages()[2]._to)
        Reporter.clear_messages()

    def test_reporter_config_conflict_only(self):
        msgs = one_of_each_kind()
        r = Reporter(send_conflict_messages=True,
                     send_admin_reports=False)
        r.add_messages(msgs)
        self.assertEqual(2, len(Reporter.get_messages()))
        self.assertIsInstance(Reporter.get_messages()[0], KeyConflictMessage)
        self.assertEqual('foo@example.org', Reporter.get_messages()[0]._to)
        self.assertIsInstance(Reporter.get_messages()[1], UserConflictMessage)
        self.assertEqual('bar@example.org', Reporter.get_messages()[1]._to)
        Reporter.clear_messages()

    def test_reporter_config_admin_only(self):
        msgs = one_of_each_kind()
        r = Reporter(send_conflict_messages=False,
                     send_admin_reports=True)
        r.add_messages(msgs)
        self.assertEqual(1, len(Reporter.get_messages()))
        self.assertIsInstance(Reporter.get_messages()[-1], AdminReport)
        self.assertEqual('admin@example.org', Reporter.get_messages()[-1]._to)
        Reporter.clear_messages()

    def test_reporter_config_all_disabled(self):
        msgs = one_of_each_kind()
        r = Reporter(send_conflict_messages=False,
                     send_admin_reports=False)
        r.add_messages(msgs)
        self.assertEqual(0, len(Reporter.get_messages()))

    def test_reporter_null_message(self):
        r = Reporter(send_conflict_messages=True,
                     send_admin_reports=True)
        r.add_messages([None])
        self.assertEqual(0, len(Reporter.get_messages()))
        Reporter.clear_messages()

    def test_admin_report_nokey(self):
        sub = SchleuderSubscriber(1, 'foo@example.org', None, 1, datetime.utcnow())
        with self.assertRaises(pgpy.errors.PGPError):
            AdminReport(
                schleuder='test@example.org',
                mail_to='admin@example.org',
                mail_from='test-owner@example.org',
                encrypt_to=BROKENKEY,
                subscribed={sub},
                unsubscribed={},
                updated={},
                added={},
                removed={},
                conflicts=[],
                sourcemap={1: 'test@example.org'})
