
import datetime
import json
import unittest

import pgpy  # type: ignore
from dateutil.tz import tzutc

from multischleuder.types import SchleuderKey, SchleuderList, SchleuderSubscriber
from multischleuder.test.test_api import _KEY_RESPONSE, _LIST_RESPONSE, _SUBSCRIBER_RESPONSE


class TestSchleuderTypes(unittest.TestCase):

    def test_parse_list(self):
        s = SchleuderList.from_api(**json.loads(_LIST_RESPONSE)[0])
        self.assertEqual(42, s.id)
        self.assertEqual('test@schleuder.example.org', s.name)
        self.assertEqual('5DCC2536D27521884A2FC7962DFD8DAAC73CB9FD', s.fingerprint)

    def test_parse_key(self):
        k = SchleuderKey.from_api(42, **json.loads(_KEY_RESPONSE))
        self.assertEqual('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9', k.fingerprint)
        self.assertEqual('andy.example@example.org', k.email)
        self.assertEqual(42, k.schleuder)
        self.assertTrue(k.blob.strip().startswith('-----BEGIN PGP PUBLIC KEY BLOCK-----'))
        self.assertTrue(k.blob.strip().endswith('-----END PGP PUBLIC KEY BLOCK-----'))
        self.assertIn('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9 (andy.example@example.org)', str(k))

    def test_parse_subscriber(self):
        k = SchleuderKey.from_api(42, **json.loads(_KEY_RESPONSE))
        s = SchleuderSubscriber.from_api(k, **json.loads(_SUBSCRIBER_RESPONSE)[0])
        self.assertEqual(23, s.id)
        self.assertEqual('andy.example@example.org', str(s))
        self.assertEqual('andy.example@example.org', s.email)
        self.assertEqual(42, s.schleuder)
        self.assertEqual(datetime.datetime(2022, 4, 15, 1, 11, 12, 123000, tzinfo=tzutc()),
                         s.created_at)
        self.assertEqual('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9', s.key.fingerprint)
        self.assertIn('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9 (andy.example@example.org)', str(k))

    def test_parse_subscriber_nokey(self):
        s = SchleuderSubscriber.from_api(None, **json.loads(_SUBSCRIBER_RESPONSE)[0])
        self.assertEqual(23, s.id)
        self.assertEqual('andy.example@example.org', str(s))
        self.assertEqual('andy.example@example.org', s.email)
        self.assertEqual(42, s.schleuder)
        self.assertEqual(datetime.datetime(2022, 4, 15, 1, 11, 12, 123000, tzinfo=tzutc()),
                         s.created_at)
        self.assertIsNone(s.key)
