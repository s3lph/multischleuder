
import io
import json
import unittest
from datetime import datetime, timedelta
from unittest.mock import patch, mock_open, MagicMock

import pgpy  # type: ignore
from dateutil.tz import tzutc

from multischleuder.conflict import KeyConflictResolution
from multischleuder.reporting import UserConflictMessage
from multischleuder.types import SchleuderKey, SchleuderList, SchleuderSubscriber


# 2FBBC0DF97FDBF1E4B704EEDE39EF4FAC420BEB6
_PRIVKEY_1, _ = pgpy.PGPKey.from_blob('''
-----BEGIN PGP PRIVATE KEY BLOCK-----

lFgEYlirsBYJKwYBBAHaRw8BAQdAGAHsSb3b3x+V6d7XouOXJryqW4mcjn1nDT2z
Fgf5lEwAAPoCqlVJWb79nANzKDdH8/mJCl5UT0CEoWyuAWtr89ofEw7ltD1NdWx0
aXNjaGxldWRlciBUZXN0IEtleSAoVEVTVCAtIERPIE5PVCBVU0UpIDxmb29AZXhh
bXBsZS5vcmc+iJAEExYIADgWIQQvu8Dfl/2/HktwTu3jnvT6xCC+tgUCYlirsAIb
AwULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAKCRDjnvT6xCC+tlGVAP0a0Yqoc/nm
OTwV1rzczdhYy9Gk7K2z7I0N380NJU4UJAEA/rVYy38ePeTl5/2NjcJ2WirTPNBT
wbMtgtLtKxHergCcXQRiWKuwEgorBgEEAZdVAQUBAQdAOcauX1G9YtgMr27fmRYM
cfji9yk9dgJEpC3GgHkYynEDAQgHAAD/XhxqpdVzZHl/Rce4VCSAq1b1LWRMYyYH
MveBRrkMuMgPgIh4BBgWCAAgFiEEL7vA35f9vx5LcE7t4570+sQgvrYFAmJYq7AC
GwwACgkQ4570+sQgvrbiUQD+LKfQo1THwAqtIwunslrCaxP64PalzDW4fepk8cyN
reAA/3X/W8pDxfm0RjOUT069Wq1/0RJAEuPPExowR25vmqIF
=b9XF
-----END PGP PRIVATE KEY BLOCK-----
''')


# 135AFA0FB3FF584828911208B7913308392972A4
_PRIVKEY_2, _ = pgpy.PGPKey.from_blob('''
-----BEGIN PGP PRIVATE KEY BLOCK-----

lFgEYljycBYJKwYBBAHaRw8BAQdA8PMUGJJ4oiYo6wwYviH798WOKKQMQJyIqhyu
URqE/b0AAP4wpWZo8GPyB7+I8qQbOzwwb+gdKTmp0WvE1P2QhxIpYRCPtEhNdWx0
aXNjaGxldWRlciBDb25mbGljdCBUZXN0IEtleSAoVEVTVCAyIC0gRE8gTk9UIFVT
RSkgPGZvb0BleGFtcGxlLm9yZz6IkAQTFggAOBYhBBNa+g+z/1hIKJESCLeRMwg5
KXKkBQJiWPJwAhsDBQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJELeRMwg5KXKk
xasA/1Pdb8eiLXTdqAMOI8H8BwEvLebiOFYL8eJx1AyjZy/vAQCNxlK4Z5cA6KzW
Zwe51YuCF69QBRatAGLhx8PoWB0DApxdBGJY8nASCisGAQQBl1UBBQEBB0DuWMns
ibefPCLJvR/LCfwRDhI3IC5W7S1506lZli3MSwMBCAcAAP9Ax8BOzTa4ewZLvO+z
2l5NBEddpKZ6q3NFKbmhmtQ3OBCtiHgEGBYIACAWIQQTWvoPs/9YSCiREgi3kTMI
OSlypAUCYljycAIbDAAKCRC3kTMIOSlypBVMAP9Uu0Hr4bJyl35WA5I7hrC666Hr
QBzu2Swgk6MkU45SLQD+LagpBVJxHcbvmK+n8MFvTSrusF8H78P4TrMLP4Onvw4=
=UiF7
-----END PGP PRIVATE KEY BLOCK-----
''')


_KEY_TEMPLATE = '''{{
  "subscriber": "{subscriber}",
  "schleuder": "{schleuder}",
  "chosen": "{chosen}"
}}'''
_USER_TEMPLATE = '''{{
  "fingerprint": "{fingerprint}",
  "subscriber": "{subscriber}",
  "schleuder": "{schleuder}",
  "chosen": "{chosen}"
}}'''


_CONFLICT_STATE_NONE = '{}'
_CONFLICT_STATE_STALE = '''{
  "53e707b460c062a2e705f59750f9297dae002a17": 123456
}'''
_CONFLICT_STATE_RECENT = f'''{{
  "53e707b460c062a2e705f59750f9297dae002a17": {int((datetime.utcnow() - timedelta(hours=6)).timestamp())}
}}'''


class TestKeyConflictResolution(unittest.TestCase):

    def test_order_resistent_key_hash(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)
        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)

        digest1 = kcr._make_key_digest(sub2, [sub1, sub2])
        digest2 = kcr._make_key_digest(sub2, [sub2, sub1])
        digest3 = kcr._make_key_digest(sub1, [sub1, sub2])
        digest4 = kcr._make_key_digest(sub1, [sub2, sub1])

        self.assertEqual(digest1, digest2)
        self.assertNotEqual(digest1, digest3)
        self.assertEqual(digest3, digest4)

    def test_order_resistent_user_hash(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'bar@example.org', str(_PRIVKEY_1.pubkey), sch2.id)
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'bar@example.org', key2, sch2.id, date2)
        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)

        digest1 = kcr._make_user_digest(sub2, [sub1, sub2])
        digest2 = kcr._make_user_digest(sub2, [sub2, sub1])
        digest3 = kcr._make_user_digest(sub1, [sub1, sub2])
        digest4 = kcr._make_user_digest(sub1, [sub2, sub1])

        self.assertEqual(digest1, digest2)
        self.assertNotEqual(digest1, digest3)
        self.assertEqual(digest3, digest4)

    def test_empty(self):
        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_NONE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_NONE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve('', '', [], {})
        self.assertEqual(0, len(resolved))
        self.assertEqual(0, len(messages))

    def test_one(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        resolved, messages = kcr.resolve('', '', [sub1], {42: sch1})
        self.assertEqual(1, len(resolved))
        self.assertEqual(sub1, resolved[0])
        self.assertEqual(0, len(messages))

    def test_same_keys_conflict(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)

        # Same key
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch2.id)
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_NONE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_NONE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})

        self.assertEqual(1, len(resolved))
        self.assertEqual('2FBBC0DF97FDBF1E4B704EEDE39EF4FAC420BEB6', resolved[0].key.fingerprint)
        # Same keys, no conflict message
        self.assertEqual(0, len(messages))

    def test_different_keys_conflict(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)

        # This subscription is older, so its key will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_NONE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_NONE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})

        self.assertEqual(1, len(resolved))
        self.assertEqual('135AFA0FB3FF584828911208B7913308392972A4', resolved[0].key.fingerprint)
        # Different keys should trigger a conflict message
        self.assertEqual(1, len(messages))
        msg = messages[0].mime
        pgp = pgpy.PGPMessage.from_blob(msg.get_payload()[1].get_payload(decode=True))
        # Verify that the message is encrypted with both keys
        dec1 = _PRIVKEY_1.decrypt(pgp)
        dec2 = _PRIVKEY_2.decrypt(pgp)
        self.assertEqual(dec1.message, dec2.message)

        payload = json.loads(dec1.message)
        self.assertEqual('foo@example.org', payload['subscriber'])
        self.assertEqual('test@schleuder.example.org', payload['schleuder'])
        self.assertEqual('135AFA0FB3FF584828911208B7913308392972A4  foo@example.org', payload['chosen'])

    def test_conflict_one_nullkey(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)

        # This subscription has no key, it it will be discarded
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'foo@example.org', None, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_NONE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_NONE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})

        self.assertEqual(1, len(resolved))
        self.assertEqual('2FBBC0DF97FDBF1E4B704EEDE39EF4FAC420BEB6', resolved[0].key.fingerprint)
        # Null key should not trigger a confict
        self.assertEqual(0, len(messages))

    def test_conflict_only_nullkeys(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', None, sch1.id, date1)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_NONE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_NONE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1],
                sourcemap={42: sch1})
        self.assertEqual(0, len(resolved))
        self.assertEqual(0, len(messages))

    def test_different_emails_conflict(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)

        # This subscription is older, so it will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'bar@example.org', str(_PRIVKEY_1.pubkey), sch2.id)
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'bar@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_NONE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_NONE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})

        self.assertEqual(1, len(resolved))
        self.assertEqual('bar@example.org', resolved[0].email)
        # Different emails should trigger conflict messages
        self.assertEqual(2, len(messages))
        self.assertIsInstance(messages[0], UserConflictMessage)
        self.assertIsInstance(messages[1], UserConflictMessage)
        msg1 = messages[0].mime
        pgp1 = pgpy.PGPMessage.from_blob(msg1.get_payload()[1].get_payload(decode=True))
        msg2 = messages[1].mime
        pgp2 = pgpy.PGPMessage.from_blob(msg2.get_payload()[1].get_payload(decode=True))
        # Verify that the message is encrypted
        dec1 = _PRIVKEY_1.decrypt(pgp1)
        dec2 = _PRIVKEY_1.decrypt(pgp2)
        self.assertNotEqual(dec1.message, dec2.message)

        payload1 = json.loads(dec1.message)
        payload2 = json.loads(dec2.message)
        if payload1['subscriber'] != 'foo@example.org':
            payload1, payload2 = payload2, payload1
        self.assertEqual('foo@example.org', payload1['subscriber'])
        self.assertEqual('2FBBC0DF97FDBF1E4B704EEDE39EF4FAC420BEB6', payload1['fingerprint'])
        self.assertEqual('test@schleuder.example.org', payload1['schleuder'])
        self.assertEqual('bar@example.org', payload1['chosen'])
        self.assertEqual('bar@example.org', payload2['subscriber'])
        self.assertEqual('2FBBC0DF97FDBF1E4B704EEDE39EF4FAC420BEB6', payload2['fingerprint'])
        self.assertEqual('test@schleuder.example.org', payload2['schleuder'])
        self.assertEqual('bar@example.org', payload2['chosen'])

    def test_zigzag_conflict(self):
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())

        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'bar@example.org', str(_PRIVKEY_2.pubkey), sch1.id)
        sub1 = SchleuderSubscriber(1, 'foo@example.org', key1, sch1.id, date2)
        sub2 = SchleuderSubscriber(2, 'bar@example.org', key2, sch1.id, date2)

        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key3 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'bar@example.org', str(_PRIVKEY_1.pubkey), sch2.id)
        sub3 = SchleuderSubscriber(7, 'bar@example.org', key3, sch2.id, date1)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_NONE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_NONE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2, sub3],
                sourcemap={42: sch1, 23: sch2})

        self.assertEqual(2, len(resolved))
        foo, bar = resolved
        if foo.email != 'foo@example.org':
            foo, bar = bar, foo
        self.assertEqual('foo@example.org', foo.email)
        self.assertEqual('2FBBC0DF97FDBF1E4B704EEDE39EF4FAC420BEB6', foo.key.fingerprint)
        self.assertEqual('bar@example.org', bar.email)
        self.assertEqual('135AFA0FB3FF584828911208B7913308392972A4', bar.key.fingerprint)
        # Different emails should trigger conflict messages
        self.assertEqual(2, len(messages))
        self.assertIsInstance(messages[0], UserConflictMessage)
        self.assertIsInstance(messages[1], UserConflictMessage)
        msg1 = messages[0].mime
        pgp1 = pgpy.PGPMessage.from_blob(msg1.get_payload()[1].get_payload(decode=True))
        msg2 = messages[1].mime
        pgp2 = pgpy.PGPMessage.from_blob(msg2.get_payload()[1].get_payload(decode=True))
        # Verify that the message is encrypted
        dec1 = _PRIVKEY_1.decrypt(pgp1)
        dec2 = _PRIVKEY_1.decrypt(pgp2)
        self.assertNotEqual(dec1.message, dec2.message)

        payload1 = json.loads(dec1.message)
        payload2 = json.loads(dec2.message)
        if payload1['subscriber'] != 'foo@example.org':
            payload1, payload2 = payload2, payload1
        self.assertEqual('foo@example.org', payload1['subscriber'])
        self.assertEqual('2FBBC0DF97FDBF1E4B704EEDE39EF4FAC420BEB6', payload1['fingerprint'])
        self.assertEqual('test@schleuder.example.org', payload1['schleuder'])
        self.assertEqual('foo@example.org', payload1['chosen'])
        self.assertEqual('bar@example.org', payload2['subscriber'])
        self.assertEqual('2FBBC0DF97FDBF1E4B704EEDE39EF4FAC420BEB6', payload2['fingerprint'])
        self.assertEqual('test@schleuder.example.org', payload2['schleuder'])
        self.assertEqual('foo@example.org', payload2['chosen'])

    def test_send_messages_nofile(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        # This subscription is older, so its key will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/nonexistent/directory/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        resolved, msgs = kcr.resolve(
            target='test@schleuder.example.org',
            mail_from='test-owner@schleuder.example.org',
            subscriptions=[sub1, sub2],
            sourcemap={42: sch1, 23: sch2})
        self.assertEqual(0, len(msgs))

    def test_send_messages_brokenstate(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        # This subscription is older, so its key will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO('[[intentionally/broken\\json]]')
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data='[[intentionally/broken\\json]]')) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, msgs = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})
        self.assertEqual(0, len(msgs))

    def test_send_messages_emptystate(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        # This subscription is older, so its key will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO()
        with patch('builtins.open', mock_open(read_data='')) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, msgs = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})
        self.assertEqual(1, len(msgs))

        now = datetime.utcnow().timestamp()
        mock_statefile.assert_called_with('/tmp/state.json', 'a+')
        contents.seek(0)
        state = json.loads(contents.read())
        self.assertEqual(1, len(state))
        self.assertIn(msgs[0].mime['X-MultiSchleuder-Digest'], state)
        self.assertLess(now - state[msgs[0].mime['X-MultiSchleuder-Digest']], 60)

    def test_send_messages_nostate(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        # This subscription is older, so its key will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_NONE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_NONE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, msgs = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})
        self.assertEqual(1, len(msgs))

        now = datetime.utcnow().timestamp()
        mock_statefile.assert_called_with('/tmp/state.json', 'a+')
        contents.seek(0)
        state = json.loads(contents.read())
        self.assertEqual(1, len(state))
        self.assertIn(msgs[0].mime['X-MultiSchleuder-Digest'], state)
        self.assertLess(now - state[msgs[0].mime['X-MultiSchleuder-Digest']], 60)

    def test_send_messages_stalestate(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        # This subscription is older, so its key will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_STALE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_STALE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})
        self.assertEqual(1, len(messages))
        msg = messages[0]

        now = datetime.utcnow().timestamp()
        mock_statefile.assert_called_with('/tmp/state.json', 'a+')
        contents.seek(0)
        state = json.loads(contents.read())
        self.assertEqual(1, len(state))
        self.assertIn(msg.mime['X-MultiSchleuder-Digest'], state)
        self.assertLess(now - state[msg.mime['X-MultiSchleuder-Digest']], 60)

    def test_send_messages_recentstate(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        # This subscription is older, so its key will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(86400, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_RECENT)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_RECENT)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})
        self.assertEqual(0, len(messages))

        now = datetime.utcnow().timestamp()
        mock_statefile.assert_called_with('/tmp/state.json', 'a+')
        # Statefile should not have been updated
        contents.seek(0)
        state = json.loads(contents.read())
        self.assertEqual(1, len(state))
        for then in state.values():
            self.assertLess(now - then, 86460)
            self.assertGreater(now - then, 60)

    def test_send_messages_dryrun(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        # This subscription is older, so its key will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        kcr.dry_run()
        contents = io.StringIO(_CONFLICT_STATE_STALE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_STALE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2],
                sourcemap={42: sch1, 23: sch2})
        self.assertEqual(1, len(messages))

        now = datetime.utcnow().timestamp()
        mock_statefile.assert_called_with('/tmp/state.json', 'a+')
        mock_statefile().write.assert_not_called()
        contents.seek(0)
        self.assertEqual(_CONFLICT_STATE_STALE, contents.read())

    def test_send_messages_nullkey(self):
        sch1 = SchleuderList(42, 'test-north@schleuder.example.org', '474777DA74528A7021184C8A0017324A6CFFBF92')
        key1 = SchleuderKey(_PRIVKEY_1.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_1.pubkey), sch1.id)
        date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
        sub1 = SchleuderSubscriber(3, 'foo@example.org', key1, sch1.id, date1)
        # This subscription is older, so its key will be preferred
        sch2 = SchleuderList(23, 'test-south@schleuder.example.org', 'AF586C0625CF77BBB659747515D41C5D84BF99D3')
        key2 = SchleuderKey(_PRIVKEY_2.fingerprint.replace(' ', ''), 'foo@example.org', str(_PRIVKEY_2.pubkey), sch2.id)
        date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
        sub2 = SchleuderSubscriber(7, 'foo@example.org', key2, sch2.id, date2)
        # This subscription does not have a key, so it will not be considered
        sch3 = SchleuderList(7, 'test-east@schleuder.example.org', '36C545D9C88AD0E7EFEF18DE06ACC53D5820EBC0')
        date3 = datetime(2022, 4, 11, 5, 23, 42, 0, tzinfo=tzutc())
        sub3 = SchleuderSubscriber(7, 'foo@example.org', None, sch3.id, date3)

        kcr = KeyConflictResolution(3600, '/tmp/state.json', _KEY_TEMPLATE, _USER_TEMPLATE)
        contents = io.StringIO(_CONFLICT_STATE_NONE)
        contents.seek(io.SEEK_END)  # Opened with 'a+'
        with patch('builtins.open', mock_open(read_data=_CONFLICT_STATE_NONE)) as mock_statefile:
            mock_statefile().__enter__.return_value = contents
            resolved, messages = kcr.resolve(
                target='test@schleuder.example.org',
                mail_from='test-owner@schleuder.example.org',
                subscriptions=[sub1, sub2, sub3],
                sourcemap={42: sch1, 23: sch2, 7: sch1})
        self.assertEqual(1, len(messages))
        msg = messages[0].mime
        pgp = pgpy.PGPMessage.from_blob(msg.get_payload()[1].get_payload(decode=True))
        # Verify that the message is encrypted with both keys
        self.assertEqual(2, len(pgp.encrypters))
        dec1 = _PRIVKEY_1.decrypt(pgp)
        dec2 = _PRIVKEY_2.decrypt(pgp)
        self.assertEqual(dec1.message, dec2.message)
