
import asyncio
import unittest
from datetime import datetime
from email.mime.text import MIMEText

from aiosmtpd.controller import Controller
from aiosmtpd.smtp import AuthResult, SMTP

from multischleuder.reporting import KeyConflictMessage, AdminReport
from multischleuder.smtp import SmtpClient, TlsMode
from multischleuder.types import SchleuderSubscriber


class MockSmtpHandler:

    def __init__(self):
        self.rcpt = []
        self.connected = False

    async def handle_HELO(self, server, session, envelope, hostname):
        self.connected = True
        session.host_name = hostname
        return '250 dummy.example.org'

    async def handle_EHLO(self, server, session, envelope, hostname, responses):
        self.connected = True
        session.host_name = hostname
        return [
            '250-dummy.example.org',
            '250-AUTH PLAIN LOGIN',
            '250-AUTH=PLAIN LOGIN',
            '250 HELP'
        ]

    async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
        self.rcpt.append(address)
        envelope.rcpt_tos.append(address)
        return '250 OK'


class MockController(Controller):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.received_user = None
        self.received_pass = None

    def _auth(self, server, session, envelope, mechanism, auth_data):
        self.received_user = auth_data.login.decode()
        self.received_pass = auth_data.password.decode()
        return AuthResult(success=True)

    def factory(self):
        return SMTP(handler=self.handler,
                    loop=self.loop,
                    auth_require_tls=False,
                    authenticator=self._auth)


class TestSmtpClient(unittest.TestCase):

    def test_empty(self):
        s = 'smtp://localhost:25'
        self.assertEqual(s, str(SmtpClient()))

    def test_parse_empty(self):
        s = 'smtp://localhost:25'
        self.assertEqual(s, str(SmtpClient.parse({})))

    def test_full(self):
        s = 'smtp+starttls://example@example.org:10025'
        c = SmtpClient(
            hostname='example.org',
            port=10025,
            tls=TlsMode.STARTTLS,
            username='example',
            password='supersecurepassword'
        )
        self.assertEqual(s, str(c))

    def test_full_parse(self):
        s = 'smtp+starttls://example@example.org:10025'
        c = SmtpClient.parse({
            'hostname': 'example.org',
            'port': 10025,
            'tls': 'STARTTLS',
            'username': 'example',
            'password': 'supersecurepassword'
        })
        self.assertEqual(s, str(c))

    def test_default_ports(self):
        self.assertEqual(25, SmtpClient.parse({'tls': 'PLAIN'})._port)
        self.assertEqual(465, SmtpClient.parse({'tls': 'SMTPS'})._port)
        self.assertEqual(587, SmtpClient.parse({'tls': 'STARTTLS'})._port)

    def test_send_message_dryrun(self):
        ctrl = MockController(handler=MockSmtpHandler(), hostname='127.0.0.1', port=10025)
        ctrl.start()
        client = SmtpClient(
            hostname='127.0.0.1',
            port=ctrl.port,
            username='example',
            password='supersecurepassword')
        client.dry_run()
        msg = MIMEText('Hello World!')
        msg['From'] = 'foo@example.org'
        msg['To'] = 'bar@example.org'
        with client:
            client._send_message(msg)
        ctrl.stop()
        self.assertEqual('example', ctrl.received_user)
        self.assertEqual('supersecurepassword', ctrl.received_pass)
        self.assertEqual(0, len(ctrl.handler.rcpt))

    def test_send_message_auth(self):
        ctrl = MockController(handler=MockSmtpHandler(), hostname='127.0.0.1', port=10026)
        ctrl.start()
        client = SmtpClient(
            hostname='127.0.0.1',
            port=ctrl.port,
            username='example',
            password='supersecurepassword')
        msg = MIMEText('Hello World!')
        msg['From'] = 'foo@example.org'
        msg['To'] = 'bar@example.org'
        with client:
            client._send_message(msg)
        ctrl.stop()
        self.assertEqual('example', ctrl.received_user)
        self.assertEqual('supersecurepassword', ctrl.received_pass)
        self.assertEqual('bar@example.org', ctrl.handler.rcpt[0])

    def test_send_message_noauth(self):
        ctrl = MockController(handler=MockSmtpHandler(), hostname='127.0.0.1', port=10027)
        ctrl.start()
        client = SmtpClient(hostname='127.0.0.1', port=ctrl.port)
        msg = MIMEText('Hello World!')
        msg['From'] = 'foo@example.org'
        msg['To'] = 'baz@example.org'
        with client:
            client._send_message(msg)
        ctrl.stop()
        self.assertIsNone(ctrl.received_user)
        self.assertIsNone(ctrl.received_pass)
        self.assertTrue(ctrl.handler.connected)
        self.assertEqual('baz@example.org', ctrl.handler.rcpt[0])

    def test_send_no_messages(self):
        ctrl = MockController(handler=MockSmtpHandler(), hostname='127.0.0.1', port=10028)
        ctrl.start()
        client = SmtpClient(
            hostname='127.0.0.1',
            port=ctrl.port,
            username='example',
            password='supersecurepassword')
        client.send_messages([])
        ctrl.stop()
        self.assertFalse(ctrl.handler.connected)
        self.assertIsNone(ctrl.received_user)
        self.assertIsNone(ctrl.received_pass)

    def test_send_multiple_messages(self):
        ctrl = MockController(handler=MockSmtpHandler(), hostname='127.0.0.1', port=10029)
        ctrl.start()
        client = SmtpClient(
            hostname='127.0.0.1',
            port=ctrl.port,
            username='example',
            password='supersecurepassword')
        sub = SchleuderSubscriber(1, 'foo@example.org', None, 1, datetime.utcnow())
        msg1 = KeyConflictMessage(
            schleuder='test@example.org',
            chosen=sub,
            affected=[sub],
            digest='digest',
            mail_from='test-owner@example.org',
            template='averylongmessage',
            sourcemap={1: 'foo@example.org'})
        msg2 = AdminReport(
            schleuder='test@example.org',
            mail_to='admin@example.org',
            mail_from='test-owner@example.org',
            encrypt_to=None,
            subscribed={},
            unsubscribed={sub},
            updated={},
            added={},
            removed={},
            conflicts=[],
            sourcemap={1: 'foo@example.org'})
        client.send_messages([msg1, msg2])
        ctrl.stop()
        self.assertTrue(ctrl.handler.connected)
        self.assertEqual('foo@example.org', ctrl.handler.rcpt[0])
        self.assertEqual('admin@example.org', ctrl.handler.rcpt[1])

    def test_send_dry_run(self):
        ctrl = MockController(handler=MockSmtpHandler(), hostname='127.0.0.1', port=10030)
        ctrl.start()
        client = SmtpClient(
            hostname='127.0.0.1',
            port=ctrl.port,
            username='example',
            password='supersecurepassword')
        client.dry_run()
        sub = SchleuderSubscriber(1, 'foo@example.org', None, 1, datetime.utcnow())
        msg1 = KeyConflictMessage(
            schleuder='test@example.org',
            chosen=sub,
            affected=[sub],
            digest='digest',
            mail_from='test-owner@example.org',
            template='averylongmessage',
            sourcemap={1: 'foo@example.org'})
        client.send_messages([msg1])
        ctrl.stop()
        self.assertFalse(ctrl.handler.connected)
        self.assertEqual(0, len(ctrl.handler.rcpt))
