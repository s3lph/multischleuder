
from typing import Dict, List, Tuple

import unittest
from datetime import datetime
from unittest.mock import MagicMock

from dateutil.tz import tzutc

from multischleuder.processor import MultiList
from multischleuder.reporting import Message
from multischleuder.test.test_conflict import _PRIVKEY_1
from multischleuder.types import SchleuderKey, SchleuderList, SchleuderSubscriber


def _resolve(target: str,
             mail_from: str,
             subscriptions: List[SchleuderSubscriber],
             sources: List[SchleuderList]) -> Tuple[List[SchleuderSubscriber], List[Message]]:
    # Mock conflict resolution that does not send or prepare messages
    subs: Dict[str, List[SchleuderSubscriber]] = {}
    for s in subscriptions:
        if s.key is not None:
            subs.setdefault(s.email, []).append(s)
    return [min(s, key=lambda x: x.created_at)for s in subs.values()], []


def _list_lists():
    return [
        SchleuderList(1, 'test@schleuder.example.org', '0CD581127A178F1931909B14B7CC98DA082D2A64'),
        SchleuderList(2, 'test-global@schleuder.example.org', 'F60B441065D485224E0F53EBBEAD4E67512CB645'),
        SchleuderList(3, 'test-north@schleuder.example.org', '552BB62D10424FF169498B3D183C27F29DAAFDFB'),
        SchleuderList(4, 'test-south@schleuder.example.org', '552BB62D10424FF169498B3D183C27F29DAAFDFB'),
        SchleuderList(5, 'test-east@schleuder.example.org', '552BB62D10424FF169498B3D183C27F29DAAFDFB'),
        SchleuderList(6, 'test-west@schleuder.example.org', '552BB62D10424FF169498B3D183C27F29DAAFDFB'),
        SchleuderList(7, 'test2-global@schleuder.example.org', 'A821095FB503836134D54138D3306C629C0DDFB5')
    ]


def _get_key(fpr: str, schleuder: SchleuderList):
    key1 = SchleuderKey('966842467B3254143F994D5E5C408C012D216471',
                        'admin@example.org', str(_PRIVKEY_1.pubkey), schleuder.id)
    key2 = SchleuderKey('6449FFB6EE68187962FA013B5CA2F4F51791BAF6',
                        'ada.lovelace@example.org', 'BEGIN PGP 1791BAF6', schleuder.id)
    key3 = SchleuderKey('414D3960D34730F63C74D5190EBC5A16716DEC79',
                        'alex.example@example.org', 'BEGIN PGP 716DEC79', schleuder.id)
    key4 = SchleuderKey('8258FAF8B161B3DD8F784874F73E2DDF045AE2D6',
                        'aspammer@example.org', 'BEGIN PGP 045AE2D6', schleuder.id)
    key5 = SchleuderKey('3699B7549B2A78F27AC93420FB6AD038C95558E5',
                        'anna.example@example.org', 'BEGIN PGP C95558E5', schleuder.id)
    key6 = SchleuderKey('698118749D1490C9822FA1D9600E77BC529681F9',
                        'anna.example@example.org', 'BEGIN PGP 529681F9', schleuder.id)
    key7 = SchleuderKey('BE11EE506C39B496A18A1FF56B3D3C0C9233D3E3',
                        'anotherspammer@example.org', 'BEGIN PGP 9233D3E3', schleuder.id)
    key8 = SchleuderKey('73E6C1170DA158D0139EF1F2349F17BB8C89A2B9',
                        'andy.example@example.org', 'BEGIN PGP 8C89A2B9', schleuder.id)
    key9 = SchleuderKey('C8C77172C31A796C563920C9358EBEA0F7662478',
                        'aaron.example@example.org', 'BEGIN PGP F7662478', schleuder.id)
    return {
        '966842467B3254143F994D5E5C408C012D216471': key1,
        '6449FFB6EE68187962FA013B5CA2F4F51791BAF6': key2,
        '414D3960D34730F63C74D5190EBC5A16716DEC79': key3,
        '8258FAF8B161B3DD8F784874F73E2DDF045AE2D6': key4,
        '3699B7549B2A78F27AC93420FB6AD038C95558E5': key5,
        '698118749D1490C9822FA1D9600E77BC529681F9': key6,
        'BE11EE506C39B496A18A1FF56B3D3C0C9233D3E3': key7,
        '73E6C1170DA158D0139EF1F2349F17BB8C89A2B9': key8,
        'C8C77172C31A796C563920C9358EBEA0F7662478': key9,
    }[fpr]


def _get_admins(schleuder: SchleuderList):
    if schleuder.id != 2:
        return []
    key = SchleuderKey('966842467B3254143F994D5E5C408C012D216471',
                       'admin@example.org', str(_PRIVKEY_1.pubkey), schleuder.id)
    date = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
    admin = SchleuderSubscriber(0, 'admin@example.org', key, schleuder.id, date)
    return [admin]


def _get_subs(schleuder: SchleuderList):
    key1 = SchleuderKey('966842467B3254143F994D5E5C408C012D216471',
                        'admin@example.org', str(_PRIVKEY_1.pubkey), schleuder.id)
    key2 = SchleuderKey('6449FFB6EE68187962FA013B5CA2F4F51791BAF6',
                        'ada.lovelace@example.org', 'BEGIN PGP 1791BAF6', schleuder.id)
    key3 = SchleuderKey('414D3960D34730F63C74D5190EBC5A16716DEC79',
                        'alex.example@example.org', 'BEGIN PGP 716DEC79', schleuder.id)
    key4 = SchleuderKey('8258FAF8B161B3DD8F784874F73E2DDF045AE2D6',
                        'aspammer@example.org', 'BEGIN PGP 045AE2D6', schleuder.id)
    key5 = SchleuderKey('3699B7549B2A78F27AC93420FB6AD038C95558E5',
                        'anna.example@example.org', 'BEGIN PGP C95558E5', schleuder.id)
    key6 = SchleuderKey('698118749D1490C9822FA1D9600E77BC529681F9',
                        'anna.example@example.org', 'BEGIN PGP 529681F9', schleuder.id)
    key7 = SchleuderKey('BE11EE506C39B496A18A1FF56B3D3C0C9233D3E3',
                        'anotherspammer@example.org', 'BEGIN PGP 9233D3E3', schleuder.id)
    key8 = SchleuderKey('73E6C1170DA158D0139EF1F2349F17BB8C89A2B9',
                        'andy.example@example.org', 'BEGIN PGP 8C89A2B9', schleuder.id)
    key9 = SchleuderKey('C8C77172C31A796C563920C9358EBEA0F7662478',
                        'aaron.example@example.org', 'BEGIN PGP F7662478', schleuder.id)
    date1 = datetime(2022, 4, 15, 5, 23, 42, 0, tzinfo=tzutc())
    date2 = datetime(2022, 4, 13, 5, 23, 42, 0, tzinfo=tzutc())
    if schleuder.id == 2:
        return [
            # admin should be ignored (unmanaged)
            SchleuderSubscriber(0, 'admin@example.org', key1, schleuder.id, date1),
            # ada.lovelace should be unsubscribed
            SchleuderSubscriber(1, 'ada.lovelace@example.org', key2, schleuder.id, date1),
            # alex.example already subscribed - no change
            SchleuderSubscriber(2, 'alex.example@example.org', key3, schleuder.id, date1),
            # aspammer should be unsubscribed (banned)
            SchleuderSubscriber(3, 'aspammer@example.org', key4, schleuder.id, date1),
            # anna.example's key should be updated, but the subscrption should stay unchanged
            SchleuderSubscriber(4, 'anna.example@example.org', key5, schleuder.id, date1),
        ]
    elif schleuder.id == 3:
        return [
            SchleuderSubscriber(5, 'alex.example@example.org', key3, schleuder.id, date1),
            SchleuderSubscriber(6, 'aspammer@example.org', key4, schleuder.id, date1),
        ]
    elif schleuder.id == 4:
        return [
            SchleuderSubscriber(7, 'anna.example@example.org', key6, schleuder.id, date1),
            SchleuderSubscriber(8, 'anotherspammer@example.org', key7, schleuder.id, date1),
        ]
    elif schleuder.id == 5:
        return [
            SchleuderSubscriber(9, 'andy.example@example.org', key8, schleuder.id, date1),
        ]
    elif schleuder.id == 6:
        return [
            SchleuderSubscriber(10, 'aaron.example@example.org', key9, schleuder.id, date2),
            # No key, should not be subscribed
            SchleuderSubscriber(11, 'arno.example@example.org', None, schleuder.id, date1),
        ]
    else:
        return []


def _get_equal_subs(schleuder: SchleuderList):
    schleuder = SchleuderList(2, schleuder.name, schleuder.fingerprint)
    subs = _get_subs(schleuder)
    return [s
            for s in subs
            if s.email not in ['admin@example.org', 'aspammer@example.org', 'anotherspammer@example.org']]


def _get_sub(email: str, schleuder: SchleuderList):
    subs = _get_subs(schleuder)
    return [s for s in subs if s.email == email][0]


class TestMultiList(unittest.TestCase):

    def _kcr_mock(self):
        kcr = MagicMock()
        kcr.resolve = _resolve
        return kcr

    def _api_mock(self, nochange=False):
        api = MagicMock()
        api.get_lists = _list_lists
        api.get_list_admins = _get_admins
        api.get_subscribers = _get_subs
        if nochange:
            api.get_subscribers = _get_equal_subs
        api.get_subscriber = _get_sub
        api.get_key = _get_key
        return api

    def test_full(self):
        sources = [
            'test-north@schleuder.example.org',
            'test-east@schleuder.example.org',
            'test-south@schleuder.example.org',
            'test-west@schleuder.example.org'
        ]
        api = self._api_mock()
        reporter = MagicMock()
        ml = MultiList(sources=sources,
                       target='test-global@schleuder.example.org',
                       unmanaged=['admin@example.org'],
                       banned=['aspammer@example.org', 'anotherspammer@example.org'],
                       mail_from='test-global-owner@schleuder.example.org',
                       api=api,
                       kcr=self._kcr_mock(),
                       reporter=reporter)
        ml.process()

        # Key uploads
        self.assertEqual(3, len(api.post_key.call_args_list))
        a, b, c = sorted([x[0] for x in api.post_key.call_args_list], key=lambda x: x[0].fingerprint)
        self.assertEqual(2, a[1].id)
        self.assertEqual('anna.example@example.org', a[0].email)
        self.assertEqual('698118749D1490C9822FA1D9600E77BC529681F9', a[0].fingerprint)
        self.assertEqual(2, b[1].id)
        self.assertEqual('andy.example@example.org', b[0].email)
        self.assertEqual('73E6C1170DA158D0139EF1F2349F17BB8C89A2B9', b[0].fingerprint)
        self.assertEqual(2, c[1].id)
        self.assertEqual('aaron.example@example.org', c[0].email)
        self.assertEqual('C8C77172C31A796C563920C9358EBEA0F7662478', c[0].fingerprint)

        # Subscriptions
        self.assertEqual(2, len(api.subscribe.call_args_list))
        a, b = sorted([x[0] for x in api.subscribe.call_args_list], key=lambda x: x[0].email)
        self.assertEqual(2, a[1].id)
        self.assertEqual('aaron.example@example.org', a[0].email)
        self.assertEqual('C8C77172C31A796C563920C9358EBEA0F7662478', a[0].key.fingerprint)
        self.assertEqual(2, b[1].id)
        self.assertEqual('andy.example@example.org', b[0].email)
        self.assertEqual('73E6C1170DA158D0139EF1F2349F17BB8C89A2B9', b[0].key.fingerprint)

        # Key updates
        self.assertEqual(1, len(api.update_fingerprint.call_args_list))
        a = api.update_fingerprint.call_args_list[0][0]
        self.assertEqual(2, a[1].id)
        self.assertEqual('anna.example@example.org', a[0].email)
        self.assertEqual('698118749D1490C9822FA1D9600E77BC529681F9', a[0].key.fingerprint)

        # Unsubscribes
        self.assertEqual(2, len(api.unsubscribe.call_args_list))
        a, b = sorted([x[0] for x in api.unsubscribe.call_args_list], key=lambda x: x[0].email)
        self.assertEqual(2, a[1].id)
        self.assertEqual('ada.lovelace@example.org', a[0].email)
        self.assertEqual(2, b[1].id)
        self.assertEqual('aspammer@example.org', b[0].email)

        # Key deletions
        self.assertEqual(3, len(api.delete_key.call_args_list))
        a, b, c = sorted([x[0] for x in api.delete_key.call_args_list], key=lambda x: x[0].fingerprint)
        self.assertEqual(2, a[1].id)
        self.assertEqual('anna.example@example.org', a[0].email)
        self.assertEqual('3699B7549B2A78F27AC93420FB6AD038C95558E5', a[0].fingerprint)
        self.assertEqual(2, b[1].id)
        self.assertEqual('ada.lovelace@example.org', b[0].email)
        self.assertEqual('6449FFB6EE68187962FA013B5CA2F4F51791BAF6', b[0].fingerprint)
        self.assertEqual(2, c[1].id)
        self.assertEqual('aspammer@example.org', c[0].email)
        self.assertEqual('8258FAF8B161B3DD8F784874F73E2DDF045AE2D6', c[0].fingerprint)

        # Todo: check message queue

    def test_no_changes(self):
        sources = [
            'test-north@schleuder.example.org',
            'test-east@schleuder.example.org',
            'test-south@schleuder.example.org',
            'test-west@schleuder.example.org'
        ]
        api = self._api_mock(nochange=True)
        reporter = MagicMock()
        ml = MultiList(sources=sources,
                       target='test-global@schleuder.example.org',
                       unmanaged=['admin@example.org'],
                       banned=['aspammer@example.org', 'anotherspammer@example.org'],
                       mail_from='test-global-owner@schleuder.example.org',
                       api=api,
                       kcr=self._kcr_mock(),
                       reporter=reporter)
        ml.process()
        # Key uploads
        self.assertEqual(0, len(api.post_key.call_args_list))
        # Subscriptions
        self.assertEqual(0, len(api.subscribe.call_args_list))
        # Key updates
        self.assertEqual(0, len(api.update_fingerprint.call_args_list))
        # Unsubscribes
        self.assertEqual(0, len(api.unsubscribe.call_args_list))
        # Key deletions
        self.assertEqual(0, len(api.delete_key.call_args_list))
