
import unittest
import urllib.error
from datetime import datetime
from unittest.mock import patch, MagicMock

from dateutil.tz import tzutc

from multischleuder.api import SchleuderApi
from multischleuder.types import SchleuderKey, SchleuderList, SchleuderSubscriber


_LIST_RESPONSE = '''
[
  {
    "id": 42,
    "created_at": "2022-04-15T01:11:12.123Z",
    "updated_at": "2022-04-15T01:11:12.123Z",
    "email": "test@schleuder.example.org",
    "fingerprint": "5DCC2536D27521884A2FC7962DFD8DAAC73CB9FD",
    "log_level": "warn",
    "subject_prefix": "",
    "subject_prefix_in": "",
    "subject_prefix_out": "",
    "openpgp_header_preference": "signencrypt",
    "public_footer": "Just a test",
    "headers_to_meta": [
      "from",
      "to",
      "cc",
      "date",
      "sig",
      "enc"
    ],
    "bounces_drop_on_headers": {},
    "keywords_admin_only": [
      "subscribe",
      "unsubscribe",
      "delete-key"
    ],
    "keywords_admin_notify": [
      "add-key"
    ],
    "send_encrypted_only": true,
    "receive_encrypted_only": true,
    "receive_signed_only": true,
    "receive_authenticated_only": true,
    "receive_from_subscribed_emailaddresses_only": false,
    "receive_admin_only": false,
    "keep_msgid": true,
    "bounces_drop_all": false,
    "bounces_notify_admins": true,
    "include_list_headers": true,
    "include_openpgp_header": true,
    "max_message_size_kb": 10240,
    "language": "en",
    "forward_all_incoming_to_admins": false,
    "logfiles_to_keep": 2,
    "internal_footer": "",
    "deliver_selfsent": true,
    "include_autocrypt_header": true,
    "set_reply_to_to_sender": false,
    "munge_from": false
  }
]
'''

_SUBSCRIBER_RESPONSE = '''
[
  {
    "id": 23,
    "list_id": 42,
    "email": "andy.example@example.org",
    "fingerprint": "ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9",
    "admin": true,
    "delivery_enabled": true,
    "created_at": "2022-04-15T01:11:12.123Z",
    "updated_at": "2022-04-15T01:11:12.123Z"
  }
]
'''

_SUBSCRIBER_RESPONSE_NOKEY = '''
[
  {
    "id": 24,
    "list_id": 42,
    "email": "andy.example@example.org",
    "fingerprint": "",
    "admin": false,
    "delivery_enabled": true,
    "created_at": "2022-04-15T01:11:12.123Z",
    "updated_at": "2022-04-15T01:11:12.123Z"
  }
]
'''

_KEY_RESPONSE = '''
{
  "fingerprint": "ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9",
  "email": "andy.example@example.org",
  "expiry": null,
  "generated_at": "2022-04-16T23:19:24.000Z",
  "primary_uid": "Mutlischleuder Test User <andy.example@example.org>",
  "oneline": "0xADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9 andy.example@example.org 2022-04-16",
  "trust_issues": null,
  "description": "pub    256?/ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9 2022-04-16\\nuid\\t\\tMutlischleuder Test User <andy.example@example.org>\\nsub    256?/ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9 2022-04-16\\nsub    256?/C0E8ED7A32F53626F2FCDC65F5035A1D90E35CAE 2022-04-16\\n",
  "ascii": "pub    256?/ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9 2022-04-16\\nuid\\t\\tMutlischleuder Test User <andy.example@example.org>\\nsub    256?/ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9 2022-04-16\\nsub    256?/C0E8ED7A32F53626F2FCDC65F5035A1D90E35CAE 2022-04-16\\n\\n\\n-----BEGIN PGP PUBLIC KEY BLOCK-----\\n\\nmDMEYlsHSBYJKwYBBAHaRw8BAQdAhGNoFKTXFsAOR8xiC7WWDB4gv+TZq5tmPG7X\\n8C3h4my0SU11dGxpc2NobGV1ZGVyIFRlc3QgVXNlciAoVEVTVCBLRVkgRE8gTk9U\\nIFVTRSkgPGFuZHkuZXhhbXBsZUBleGFtcGxlLm9yZz6IkAQTFggAOBYhBK25vGef\\n9TzI72b6w5NI/at6dmP5BQJiWwdIAhsjBQsJCAcCBhUKCQgLAgQWAgMBAh4BAheA\\nAAoJEJNI/at6dmP54NoBAMGktGRD7fgmruTviHhERbhUX9OmPGUuH1tsUFVAsePk\\nAP0Xt8Uq876t87FIMMil7zuo7Oc/lYqS+JONd0NEOIzUD7hXBGJbB0gSCSsGAQQB\\n2kcPAQIDBHhrny0kv/i58MlgJmR0g3dyadbPGt66Yht0dY6Azkz8eAbMuPG+Gqhu\\n/txLXnzPI1Gb99i934CCFUPgsvMorEIDAQgHiHgEGBYIACAWIQStubxnn/U8yO9m\\n+sOTSP2renZj+QUCYlsHSAIbDAAKCRCTSP2renZj+R9nAQDOcZRSgl9l7Z1inKjO\\nEwaQmYg/O9xked0C5mJwlV2mdgD9Gvamm5n6djU2D91X8Wbp49upWe1rAv2EgeAQ\\na5AcmwE=\\n=RIBQ\\n-----END PGP PUBLIC KEY BLOCK-----\\n\\n"
}
'''  # noqa E501


class TestSchleuderApi(unittest.TestCase):

    def _mock_api(self, mock, nokey=False, error=None):
        m = MagicMock()
        m.getcode.return_value = 200

        def read():
            url = mock.call_args_list[-1][0][0].get_full_url()
            method = mock.call_args_list[-1][0][0].method
            if method == 'GET':
                if '/lists' in url:
                    return _LIST_RESPONSE.encode()
                if '/subscriptions' in url:
                    if nokey:
                        return _SUBSCRIBER_RESPONSE_NOKEY.encode()
                    return _SUBSCRIBER_RESPONSE.encode()
                if '/keys' in url:
                    return _KEY_RESPONSE.encode()
                return b'null'
            else:
                return b''
        m.read = read
        m.__enter__.return_value = m
        mock.return_value = m
        api = SchleuderApi('https://localhost:4443',
                           '86cf2676d065dc902548e563ab22b57868ed2eb6')
        if error is not None:
            def __request(*args, **kwargs):
                raise error
            api._SchleuderApi__request = __request

        return api

    @patch('urllib.request.urlopen')
    def test_get_lists(self, mock):
        api = self._mock_api(mock)
        lists = api.get_lists()
        self.assertEqual(1, len(lists))
        self.assertEqual('test@schleuder.example.org', lists[0].name)
        # Test request data
        self.assertEqual('https://localhost:4443/lists.json',
                         mock.call_args[0][0].get_full_url())
        self.assertEqual('Basic c2NobGV1ZGVyOjg2Y2YyNjc2ZDA2NWRjOTAyNTQ4ZTU2M2FiMjJiNTc4NjhlZDJlYjY=',
                         mock.call_args[0][0].headers.get('Authorization'))

    @patch('urllib.request.urlopen')
    def test_get_list_admins(self, mock):
        api = self._mock_api(mock, nokey=True)
        admins = api.get_list_admins(SchleuderList(42, '', ''))
        self.assertEqual(0, len(admins))
        api = self._mock_api(mock)
        admins = api.get_list_admins(SchleuderList(42, '', ''))
        self.assertEqual(1, len(admins))
        self.assertEqual(23, admins[0].id)
        self.assertEqual('andy.example@example.org', admins[0].email)
        self.assertEqual(42, admins[0].schleuder)
        self.assertEqual('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9', admins[0].key.fingerprint)

    @patch('urllib.request.urlopen')
    def test_get_subscribers(self, mock):
        api = self._mock_api(mock)
        subs = api.get_subscribers(SchleuderList(42, '', ''))
        # Test request data
        self.assertEqual('https://localhost:4443/subscriptions.json?list_id=42',
                         mock.call_args_list[0][0][0].get_full_url())
        self.assertEqual('https://localhost:4443/keys/ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9.json?list_id=42',
                         mock.call_args_list[1][0][0].get_full_url())
        self.assertEqual(1, len(subs))
        self.assertEqual(23, subs[0].id)
        self.assertEqual('andy.example@example.org', subs[0].email)
        self.assertEqual(42, subs[0].schleuder)
        self.assertEqual(datetime(2022, 4, 15, 1, 11, 12, 123000, tzinfo=tzutc()),
                         subs[0].created_at)
        self.assertEqual('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9', subs[0].key.fingerprint)
        self.assertEqual('andy.example@example.org', subs[0].key.email)
        self.assertIn('-----BEGIN PGP PUBLIC KEY BLOCK-----', subs[0].key.blob)
        self.assertEqual(42, subs[0].key.schleuder)

    @patch('urllib.request.urlopen')
    def test_get_subscribers_nokey(self, mock):
        api = self._mock_api(mock, nokey=True)
        subs = api.get_subscribers(SchleuderList(42, '', ''))
        # Test request data
        self.assertEqual('https://localhost:4443/subscriptions.json?list_id=42',
                         mock.call_args_list[0][0][0].get_full_url())
        self.assertEqual(1, len(subs))
        self.assertEqual(24, subs[0].id)
        self.assertEqual('andy.example@example.org', subs[0].email)
        self.assertEqual(42, subs[0].schleuder)
        self.assertEqual(datetime(2022, 4, 15, 1, 11, 12, 123000, tzinfo=tzutc()),
                         subs[0].created_at)
        self.assertIsNone(subs[0].key)

    @patch('urllib.request.urlopen')
    def test_get_subscriber(self, mock):
        api = self._mock_api(mock)
        sub = api.get_subscriber('andy.example@example.org', SchleuderList(42, '', ''))
        self.assertEqual(23, sub.id)
        self.assertEqual('andy.example@example.org', sub.email)
        self.assertEqual('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9', sub.key.fingerprint)
        self.assertEqual(42, sub.key.schleuder)
        self.assertEqual(42, sub.schleuder)
        with self.assertRaises(KeyError):
            api.get_subscriber('bar@example.org', SchleuderList(42, '', ''))

    @patch('urllib.request.urlopen')
    def test_get_subscriber_nokey(self, mock):
        api = self._mock_api(mock, nokey=True)
        sub = api.get_subscriber('andy.example@example.org', SchleuderList(42, '', ''))
        self.assertEqual(24, sub.id)
        self.assertEqual('andy.example@example.org', sub.email)
        self.assertIsNone(sub.key)

    @patch('urllib.request.urlopen')
    def test_subscribe(self, mock):
        api = self._mock_api(mock)
        now = datetime.utcnow()
        key = SchleuderKey('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9',
                           'andy.example@example.org', 'verylongpgpkeyblock', 42)
        sub = SchleuderSubscriber(23, 'andy.example@example.org', key, 42, now)
        api.subscribe(sub, SchleuderList(42, '', ''))
        self.assertEqual('https://localhost:4443/subscriptions.json?list_id=42',
                         mock.call_args_list[-1][0][0].get_full_url())
        self.assertEqual('POST', mock.call_args_list[-1][0][0].method)
        # todo assert request payload

    @patch('urllib.request.urlopen')
    def test_subscribe_nokey(self, mock):
        api = self._mock_api(mock)
        now = datetime.utcnow()
        sub = SchleuderSubscriber(23, 'andy.example@example.org', None, 42, now)
        with self.assertRaises(ValueError):
            api.subscribe(sub, SchleuderList(42, '', ''))

    @patch('urllib.request.urlopen')
    def test_unsubscribe(self, mock):
        api = self._mock_api(mock)
        now = datetime.utcnow()
        key = SchleuderKey('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9',
                           'andy.example@example.org', 'verylongpgpkeyblock', 42)
        sub = SchleuderSubscriber(23, 'andy.example@example.org', key, 42, now)
        api.unsubscribe(sub, SchleuderList(42, '', ''))
        self.assertEqual('https://localhost:4443/subscriptions/23.json',
                         mock.call_args_list[-1][0][0].get_full_url())
        self.assertEqual('DELETE', mock.call_args_list[-1][0][0].method)
        # todo assert request payload

    @patch('urllib.request.urlopen')
    def test_update_fingerprint(self, mock):
        api = self._mock_api(mock)
        now = datetime.utcnow()
        key = SchleuderKey('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9',
                           'andy.example@example.org', 'verylongpgpkeyblock', 42)
        sub = SchleuderSubscriber(23, 'andy.example@example.org', key, 42, now)
        api.update_fingerprint(sub, SchleuderList(42, '', ''))
        self.assertEqual('https://localhost:4443/subscriptions/23.json',
                         mock.call_args_list[-1][0][0].get_full_url())
        self.assertEqual('PATCH', mock.call_args_list[-1][0][0].method)
        # todo assert request payload

    @patch('urllib.request.urlopen')
    def test_update_fingerprint_nokey(self, mock):
        api = self._mock_api(mock)
        now = datetime.utcnow()
        sub = SchleuderSubscriber(23, 'andy.example@example.org', None, 42, now)
        with self.assertRaises(ValueError):
            api.update_fingerprint(sub, SchleuderList(42, '', ''))

    @patch('urllib.request.urlopen')
    def test_get_key(self, mock):
        api = self._mock_api(mock)
        key = api.get_key('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9', SchleuderList(42, '', ''))
        self.assertEqual('https://localhost:4443/keys/ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9.json?list_id=42',
                         mock.call_args_list[0][0][0].get_full_url())
        self.assertEqual('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9', key.fingerprint)
        self.assertEqual('andy.example@example.org', key.email)
        self.assertEqual(42, key.schleuder)
        self.assertIn('-----BEGIN PGP PUBLIC KEY BLOCK-----', key.blob)

    @patch('urllib.request.urlopen')
    def test_get_key_404(self, mock):
        url = 'https://localhost:4443/keys/ADB9BC679FF53CC8EF66FAC39348FDAB7A7663FA.json?list_id=42'
        api = self._mock_api(mock, error=urllib.error.HTTPError(url=url, code=404, msg='Not Found', hdrs={}, fp=None))
        key = api.get_key('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663FA', SchleuderList(42, '', ''))
        self.assertIsNone(key)

    @patch('urllib.request.urlopen')
    def test_post_key(self, mock):
        api = self._mock_api(mock)
        key = SchleuderKey('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9',
                           'andy.example@example.org', 'verylongpgpkeyblock', 42)
        api.post_key(key, SchleuderList(42, '', ''))
        self.assertEqual('https://localhost:4443/keys.json?list_id=42',
                         mock.call_args_list[0][0][0].get_full_url())
        self.assertEqual('POST', mock.call_args_list[0][0][0].method)
        # todo assert request payload

    @patch('urllib.request.urlopen')
    def test_delete_key(self, mock):
        api = self._mock_api(mock)
        key = SchleuderKey('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9',
                           'andy.example@example.org', 'verylongpgpkeyblock', 42)
        api.delete_key(key, SchleuderList(42, '', ''))
        self.assertEqual('https://localhost:4443/keys/ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9.json?list_id=42',
                         mock.call_args_list[0][0][0].get_full_url())
        self.assertEqual('DELETE', mock.call_args_list[0][0][0].method)
        # todo assert request payload

    @patch('urllib.request.urlopen')
    def test_dry_run(self, mock):
        api = self._mock_api(mock)
        api.dry_run()
        now = datetime.utcnow()
        key = SchleuderKey('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9',
                           'andy.example@example.org', 'verylongpgpkeyblock', 42)
        sub = SchleuderSubscriber(23, 'andy.example@example.org', key, 42, now)
        sch = SchleuderList(42, '', '')
        # create, update, delete should be no-ops; 5 requests for retrieving the keys & subscriptions in unsub & update
        api.subscribe(sub, sch)
        api.unsubscribe(sub, sch)
        api.update_fingerprint(sub, sch)
        api.post_key(key, SchleuderList(42, '', ''))
        api.delete_key(key, SchleuderList(42, '', ''))
        self.assertGreater(5, len(mock.call_args_list))
        # only reads should execute
        api.get_lists()
        api.get_subscribers(sch)
        api.get_key('ADB9BC679FF53CC8EF66FAC39348FDAB7A7663F9', sch)
        self.assertLess(2, len(mock.call_args_list))
