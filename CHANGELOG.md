# MultiSchleuder Changelog

<!-- BEGIN RELEASE v0.1.7 -->
## Version 0.1.7

Bugfix Release

### Changes

<!-- BEGIN CHANGES 0.1.7 -->
- Remove and re-import keys whose expiry date has been changed
- Don't report keys as changed if they appear to differ, but are treated as identical by GnuPG.
<!-- END CHANGES 0.1.7 -->

<!-- END RELEASE v0.1.7 -->

<!-- BEGIN RELEASE v0.1.6 -->
## Version 0.1.6

Bugfix Release

### Changes

<!-- BEGIN CHANGES 0.1.6 -->
- Better error handling for wrongfully configured keys
<!-- END CHANGES 0.1.6 -->

<!-- END RELEASE v0.1.6 -->

<!-- BEGIN RELEASE v0.1.5 -->
## Version 0.1.5

Reporting changes

### Changes

<!-- BEGIN CHANGES 0.1.5 -->
- Admin reports show the source sublist for each new subscriber
- Add static code analysis CI jobs
<!-- END CHANGES 0.1.5 -->

<!-- END RELEASE v0.1.5 -->

<!-- BEGIN RELEASE v0.1.4 -->
## Version 0.1.4

Better error handling

### Changes

<!-- BEGIN CHANGES 0.1.4 -->
- Processing failure in one list won't affect other lists
- Admin reports are never sent unencrypted
<!-- END CHANGES 0.1.4 -->

<!-- END RELEASE v0.1.4 -->

<!-- BEGIN RELEASE v0.1.3 -->
## Version 0.1.3

Bugfixes and small improvements in reporting

### Changes

<!-- BEGIN CHANGES 0.1.3 -->
- RFC 3156 compliance: Don't base64-encode encrypted reports
- Include conflicts in admin reports
- Fix: user conflict message emails were sent multiple times to chosen email
<!-- END CHANGES 0.1.3 -->

<!-- END RELEASE v0.1.3 -->

<!-- BEGIN RELEASE v0.1.2 -->
## Version 0.1.2

Documentation & Bugfix release

### Changes

<!-- BEGIN CHANGES 0.1.2 -->
- Commented config file
- Fix a typo in the Debian systemd service
<!-- END CHANGES 0.1.2 -->

<!-- END RELEASE v0.1.2 -->

<!-- BEGIN RELEASE v0.1.1 -->
## Version 0.1.1

Bugfix release

### Changes

<!-- BEGIN CHANGES 0.1.1 -->
- Fix logging (almost nothing was logged)
- Fix a typo in the Debian systemd timer
<!-- END CHANGES 0.1.1 -->

<!-- END RELEASE v0.1.1 -->

<!-- BEGIN RELEASE v0.1 -->
## Version 0.1

First release.

### Changes

<!-- BEGIN CHANGES 0.1 -->
- First somewhat stable version.
<!-- END CHANGES 0.1 -->

<!-- END RELEASE v0.1 -->
